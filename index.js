const fs = require('fs')
const path = require('path')

DirectoryHandler = (folder, dest) => {
  fs.readdir(folder, function(err, items) {
    let newDir = path.join(dest, path.basename(folder))

    fs.mkdirSync(path.join(dest, path.basename(folder)))

    items.map((item, index) => {
      items[index] = path.join(fs.realpathSync(folder), item)
    })

    FileHandler(items, newDir)
  })
}

FileHandler = (files, dest) => {
  files.map((file, index) => {
    fs.stat(file, function(err, stats) {
      if (stats.isDirectory()) {
        DirectoryHandler(fs.realpathSync(file), fs.realpathSync(dest))
      } else {
        fs.writeFileSync(path.join(dest, path.basename(file)), fs.readFileSync(file))
      }
    })
  })
}

function UpdateDist(options) {
  this.options = options
}

UpdateDist.prototype.apply = function(compiler) {
  var options = this.options

  compiler.plugin('done', function() {
    FileHandler(options.files, options.path)
  });
};

module.exports = UpdateDist;
