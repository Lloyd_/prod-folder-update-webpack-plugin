### What is this repository for? ###

A simple plugin that moves selected assets and folders into your production build dist folder automatically at the end of your webpack build.

This plugin is most usefull when using react and webpack together.

### How do I get set up? ###

npm i --save prod-folder-update-webpack-plugin

### Usage Example ###

```
  const ProdFolderUpdatePlugin = require('prod-folder-update-webpack-plugin');

  plugins: [
    new ProdFolderUpdatePlugin({
      files: [
        './service-worker.js',
        './manifest.json',
        // always select the outer-most parent when moving a folder,
        // the full file path will not be preserved.
        './assets'],
      path: path.join(__dirname, '/dist/')
    })
  ],

```

### Who do I talk to? ###

lloydleagas@gmail.com
